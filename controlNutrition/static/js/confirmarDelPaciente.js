function confirmarEliminacionPaciente(id){

    Swal.fire({
        title: '¿Esta seguro que desea eliminar este registro?',
        text: "No podra deshacer esta acción",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.isConfirmed) {
            window.location.href = "paciente_eliminar/"+id;            
        }
      })
  }