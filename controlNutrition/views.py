from django.shortcuts import get_object_or_404, redirect, render

from  controlNutrition.models import pacientes, medicos, usuarios

from django.contrib import messages

from tais_workbot.settings import DATABASES

def paginaIndex(request):
    return render (request, 'index.html')	

def registerPaciente(request):
    if request.method=='POST':
        nombres=request.POST['nombrePaciente']
        apellidos=request.POST['apellido']
        apoderado=request.POST['apoderado']
        celular=request.POST['celular']
        dni=request.POST['dni']
        email=request.POST['email']
        provincia=request.POST['provincia']
        distrito=request.POST['distrito']
        fechaNacimiento=request.POST['fecha']
        peso=request.POST['peso']
        talla=request.POST['talla']
        edad=request.POST['edad']
        genero=request.POST['genero']
        estado='A'

        pacientes(nombres=nombres, apellidos=apellidos, apoderado=apoderado, celular=celular,dni=dni, email=email,
                    provincia=provincia, distrito=distrito, fechaNacimiento=fechaNacimiento, peso=peso, talla=talla, 
                    edad=edad, genero=genero, estado=estado).save()
        messages.success(request, 'El paciente '+request.POST['nombrePaciente']+' se registro exitosamente')
        return render(request, 'paciente_register.html')
    else:
        accion = "registrar";
        titulo = "Nuevo Paciente";
        return render(request, 'paciente_register.html', {'accion':accion, 'titulo':titulo})


def registerNutricionista(request):
    if request.method=='POST':
        nombres=request.POST['nombre']
        apellidos=request.POST['apellido']
        profesion=request.POST['profesion']
        celular=request.POST['celular']
        telefono=request.POST['telefono']
        email=request.POST['email']
        departamento=request.POST['departamento']
        ciudad=request.POST['ciudad']
        usuario=request.POST['usuario']
        contrasenia=request.POST['usuario']
        cargo=request.POST['rol']
        estado="A"
        
        us=usuarios(usuario=usuario, contrasenia=contrasenia, cargo=cargo, estado=estado)
        us.save()
        medicos(nombres=nombres, apellidos=apellidos, profesion=profesion, celular=celular,telefono=telefono, email=email,
                    departamento=departamento, ciudad=ciudad, estado=estado, usuario_id_id=us.id).save()
        messages.success(request, 'El nutricionista '+request.POST['nombre']+' se registro exitosamente')
        return redirect('allNutricionista')
    else:
        return render(request, 'nutricionista_register.html')

def nutricionista_eliminar(request, pk):
    
    nutricionista = get_object_or_404(medicos, id=pk)
    nutricionista.delete()

    return redirect('allNutricionista')

def usuario_eliminar(request, pk):
    
    usuario = get_object_or_404(usuarios, id=pk)
    usuario.delete()

    return redirect('allNutricionista')

def paciente_eliminar(request, pk):
    
    paciente = get_object_or_404(pacientes, id=pk)
    paciente.delete()

    return redirect('allPacientes')
