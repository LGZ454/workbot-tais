from django.apps import AppConfig


class ControlnutritionConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'controlNutrition'
