from django.contrib import admin

from controlNutrition.models import medicos, pacientes, usuarios, seguimiento_pacientes,consulta_medicas, diagnosticos

# Register your models here.

class MedicosAdmin(admin.ModelAdmin):
    list_display=("nombres","apellidos","profesion")

class PacientesAdmin(admin.ModelAdmin):
    list_display=("nombres","apellidos")

class SeguimientoPacientesAdmin(admin.ModelAdmin):
    list_display=("consulta_medica_id","peso")

admin.site.register(medicos,MedicosAdmin)
admin.site.register(usuarios)
admin.site.register(pacientes,PacientesAdmin)
admin.site.register(seguimiento_pacientes,SeguimientoPacientesAdmin)
admin.site.register(consulta_medicas)
admin.site.register(diagnosticos)