from django.shortcuts import render

from  controlNutrition.models import  usuarios
from django.contrib import messages

def paginaIndex(request):
    return render (request, 'index.html')

def paginaAdmin(request):
    return render (request, 'admin.html')

def paginaLogin(request):
    if request.method=='POST':
        try:
            detalleUsuario=usuarios.objects.get(usuario=request.POST['username'], contrasenia=request.POST['password'])
            print("Usuario", detalleUsuario.usuario, detalleUsuario.contrasenia, detalleUsuario.cargo)
            request.session['usuario']=detalleUsuario.usuario
            if detalleUsuario.cargo=='Nutricionista':
                return render (request, 'index.html')
            else:
                return render (request, 'admin.html')
        except usuarios.DoesNotExist as e:
            messages.success(request, 'Datos Incorrectos, verifique usuario o contraseña')
    return render(request, 'login.html')


def paginaDiagnostico(request):
    return render(request, 'diagnostico.html')
