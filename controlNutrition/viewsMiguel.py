from django.shortcuts import render

from  controlNutrition.models import medicos
from django.contrib import messages

def tablaMedicos(request):

    AllMedicos = medicos.objects.filter(estado__in='A')

    return render (request, 'tabla_nutricionistas.html', {'nutricionistas':AllMedicos})