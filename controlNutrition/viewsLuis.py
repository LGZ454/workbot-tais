from django.shortcuts import render, redirect

from  controlNutrition.models import pacientes
from django.contrib import messages
from decimal import Decimal
import logging

def tablaPacientes(request):

	listaPacientes = pacientes.objects.filter(estado__in='A')

	return render (request, 'tabla_pacientes.html', {'pacientes':listaPacientes})

def editarPaciente(request):
	if request.method=='GET':
		mi_paciente = request.GET['pacienteid']
		paciente = pacientes.objects.get(id=mi_paciente)
		print("mi paciente")
		print(paciente.fechaNacimiento)

		return render (request, 'paciente_register.html', {'paciente':paciente, 'titulo':'Editar Paciente'})
	if request.method=='POST':
		mi_paciente = request.POST['pacienteid']
		paciente = pacientes.objects.get(id=mi_paciente)
		paciente.nombres=request.POST['nombrePaciente']
		paciente.apellidos=request.POST['apellido']
		paciente.apoderado=request.POST['apoderado']
		paciente.celular=request.POST['celular']
		paciente.dni=request.POST['dni']
		paciente.email=request.POST['email']
		paciente.provincia=request.POST['provincia']
		paciente.distrito=request.POST['distrito']
		paciente.fechaNacimiento=request.POST['fecha']
		mipeso = float(request.POST['peso'])
		mipeso = "{:.2f}".format(mipeso)
		paciente.peso=mipeso
		paciente.talla=Decimal(request.POST['talla'])
		paciente.edad=request.POST['edad']
		paciente.genero=request.POST['genero']
		paciente.save()
		return redirect('allPacientes')

def eliminarPacientes(request):
	if request.method=='POST':
		mi_paciente = request.POST['pacienteid']
		paciente = pacientes.objects.get(id=mi_paciente)
		paciente.estado='D'
		paciente.save()
		return redirect('allPacientes')
