from django.shortcuts import render, redirect

from  controlNutrition.models import medicos,usuarios
from django.contrib import messages

def tablaMedicos(request):

    AllMedicos = medicos.objects.filter(estado__in='A')

    return render (request, 'tabla_nutricionistas.html', {'nutricionistas':AllMedicos})

def editarNutricionista(request,id):
    nutricionista = medicos.objects.get(id = id)
    userMedico = usuarios.objects.get(usuario = nutricionista.usuario_id)
  
    if request.method=='GET':
        return render(request, 'nutricionista_register.html',{'medico':nutricionista,'user':userMedico})
    
    if request.method=='POST':
        nutricionista.nombres = request.POST['nombre']
        nutricionista.apellidos = request.POST['apellido']
        nutricionista.profesion = request.POST['profesion']
        nutricionista.celular = request.POST['celular']
        nutricionista.telefono = request.POST['telefono']
        nutricionista.email = request.POST['email']
        nutricionista.departamento = request.POST['departamento']
        nutricionista.ciudad = request.POST['ciudad']

        userMedico.usuario = request.POST['usuario']
        userMedico.contrasenia = request.POST['password']
        userMedico.cargo = request.POST['rol']

        nutricionista.save(update_fields=['nombres','apellidos','profesion','celular','telefono','email','departamento','ciudad'])
        userMedico.save(update_fields=['usuario','contrasenia','cargo'])
        return redirect('allNutricionista')
