from django.shortcuts import render

from  controlNutrition.models import pacientes,seguimiento_pacientes
from django.contrib import messages
from joblib import load
from decimal import Decimal
from django.http import JsonResponse

def paginaIndex(request):
    return render (request, 'index.html')

def seguimiento_paciente(request):
    seguimientos = seguimiento_pacientes.objects.all()
    # print >>sys.stderr, 'Goodbye, cruel world!'
    return render(request, 'seguimiento_paciente.html', {'seguimientos':seguimientos})

def realizar_prediccion(request):
    genero = Decimal(request.POST['genero'])
    edad = Decimal(request.POST['edad'])
    peso = float(request.POST['peso'])
    peso = "{:.2f}".format(peso)
    talla = float(request.POST['talla'])
    talla = "{:.2f}".format(talla)

    clf3 = load('modelo_joblib/Red_5000000.joblib')
    clf3 = clf3.predict([[genero,edad,peso,talla]])
    clf3 = clf3.round()
    
    print(clf3[0,0])

    PT=""
    TE=""
    PE=""

    if(clf3[0,0]<=0):
        PT="Desnutricion aguda"

    if(clf3[0,0]==1):
        PT="Normal"

    if(clf3[0,0]==2):
        PT="Sobrepeso"

    if(clf3[0,0]>=3):
        PT="Obesidad"

    
    if(clf3[0,1]<=0):
        TE="Desnutricion cronica"

    if(clf3[0,1]>=1):
        TE="Normal"

    
    if(clf3[0,2]<=0):
        PE="Desnutricion global"

    if(clf3[0,2]>=1):
        PE="Normal"


    print(TE)
    
    responseData = {
        'PT': PT,
        'TE': TE,
        'PE' : PE
    }

    return JsonResponse(responseData)