from django import forms
from django.forms.fields import EmailField
from django.forms.forms import Form

class FormularioNutricionista(forms.Form):

    nombre=forms.CharField()
    apellidos=forms.EmailField()
    profesion=forms.CharField()
    celular=forms.CharField
    telefono=forms.CharField
    email=forms.EmailField
    departamento=forms.CharField
    ciudad=forms.CharField