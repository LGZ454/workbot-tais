from django.db import models

# Create your models here.

class usuarios(models.Model):
    usuario=models.CharField(max_length=20, unique=True)
    contrasenia=models.CharField(max_length=20)
    cargo=models.CharField(max_length=20)
    estado=models.CharField(max_length=1, default='A')

    def __str__(self):
        return self.usuario


class medicos(models.Model):
    usuario_id=models.ForeignKey(usuarios,on_delete=models.CASCADE)
    apellidos=models.CharField(max_length=50)
    nombres=models.CharField(max_length=50)
    profesion=models.CharField(blank=True, max_length=20)
    celular=models.CharField(blank=True, max_length=9)
    telefono=models.CharField(blank=True, max_length=20)
    email=models.EmailField()
    departamento=models.CharField(blank=True, max_length=20)
    ciudad=models.CharField(blank=True, max_length=20)
    estado=models.CharField(max_length=1, default='A')

    def __str__(self):
        return self.nombres + " " + self.apellidos


class pacientes(models.Model):
    apellidos=models.CharField(max_length=50)
    nombres=models.CharField(max_length=50)
    apoderado=models.CharField(max_length=70)
    celular=models.CharField(max_length=9)
    dni=models.CharField(max_length=8, unique=True)
    email=models.EmailField(blank=True)
    provincia=models.CharField(blank=True, max_length=20)
    distrito=models.CharField(blank=True, max_length=20)
    fechaNacimiento=models.DateField()
    peso=models.DecimalField(max_digits=4, decimal_places=2)
    talla=models.IntegerField()
    edad=models.IntegerField()
    
    MASCULINO = 'M'
    FEMENINO = 'F'
    GENERO_PACIENTE={
    (MASCULINO, 'Masculino'),
    (FEMENINO, 'Femenino'),
    }
    genero=models.CharField(max_length=1, choices=GENERO_PACIENTE)
    
    estado=models.CharField(max_length=1, default='A')

    def __str__(self):
        return self.nombres

class consulta_medicas(models.Model):
    medico_id=models.ForeignKey(medicos,on_delete=models.CASCADE)
    paciente_id=models.ForeignKey(pacientes,on_delete=models.CASCADE)
    fechaConsulta=models.DateField()
    estado=models.CharField(max_length=1, default='A')

class seguimiento_pacientes(models.Model):
    consulta_medica_id=models.ForeignKey(consulta_medicas,on_delete=models.CASCADE)
    peso=models.DecimalField(max_digits=4, decimal_places=2)
    talla=models.IntegerField()
    edad=models.IntegerField()
    estado=models.CharField(max_length=1, default='A')

class diagnosticos(models.Model):
    consulta_medica_id=models.ForeignKey(consulta_medicas,on_delete=models.CASCADE)
    diagnostico=models.CharField(max_length=120)
    comentario=models.TextField(blank=True)
    estado=models.CharField(max_length=1, default='A')