"""tais_workbot URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from controlNutrition import views
from controlNutrition import viewsJosue
from controlNutrition import viewsMiguel
from controlNutrition import viewsWilliam
from controlNutrition import viewsDiego
from controlNutrition import viewsLuis


urlpatterns = [
    path('admin/', admin.site.urls),
    path('Inicio', views.paginaIndex, name='index'),
    path('paciente_register', views.registerPaciente, name='register'),
    path('nutricionista_register', views.registerNutricionista, name='registerNut'),
    #path('nutricionista_delete', views.eliminarNutricionista, name='deleteNut'),
    path('nutricionista_eliminar/<int:pk>', views.nutricionista_eliminar, name='nutricionista_eliminar'),
    path('usuario_eliminar/<int:pk>', views.usuario_eliminar, name='usuario_eliminar'),
    path('paciente_eliminar/<int:pk>', views.paciente_eliminar, name='paciente_eliminar'),

    path('', viewsJosue.paginaLogin, name='login'),
    path('admin', viewsJosue.paginaAdmin, name='admin'),
    path('Diagnostico', viewsJosue.paginaDiagnostico, name='diagnostico'),
    #JOSUE
    #1
    #2
    #3
    #4
    #5
    
    #MIGUEL
    path('tabla_nutricionistas', viewsMiguel.tablaMedicos, name='allNutricionista'),
    #2
    #3
    #4
    #5
    
    #WILLIAM
    path('seguimiento_paciente', viewsWilliam.seguimiento_paciente, name='seguimiento'),
    path('realizar_prediccion', viewsWilliam.realizar_prediccion, name='realizarPrediccion'),
    #3
    #4
    #5
    
    #DIEGO
    path('tabla_nutricionistas', viewsDiego.tablaMedicos, name='allNutricionista'),
    path('nutricionistas_edit/<int:id>/', viewsDiego.editarNutricionista, name='editNutricionista'),
    #3
    #4
    #5
    
    #LUIS
    path('tabla_pacientes', viewsLuis.tablaPacientes, name='allPacientes'),
    path('paciente_edit', viewsLuis.editarPaciente, name='editarPaciente'),
    #path('paciente_eliminar', viewsLuis.eliminarPacientes, name='eliminarPacientes'),
    #4
    #5

]
